from setuptools import setup

setup(name='testlib',
      version='0.0.1',
      description='Test lib package',
      url='http://github.com/storborg/funniest',
      author='Ivan lookyanow',
      author_email='lookyanow@gmail.com',
      license='MIT',
      packages=['testlib'],
      zip_safe=False)
